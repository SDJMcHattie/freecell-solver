package uk.co.iscoding.freecell.game;

import org.junit.Test;
import uk.co.iscoding.freecell.cards.DeckOfCards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

/**
 * Created with IntelliJ IDEA.
 * User: Stuart
 * Date: 28/05/2013
 * Time: 22:06
 */
public class FreeCellLayoutTest {
    @Test
    public void testIsFinished() throws Exception {

    }

    @Test
    public void testPrintPriorMoves() throws Exception {

    }

    @Test
    public void testGetPossibleMoves() throws Exception {

    }

    @Test
    public void testEqualsAndHashCode() throws Exception {
        FreeCellLayout layout1 = new FreeCellLayout(new DeckOfCards());
        FreeCellLayout layout2 = new FreeCellLayout(new DeckOfCards());

        // These will not be the same because all areas are based on sets, but should be equal
        assertNotSame("Layouts are not the same object", layout1, layout2);
        assertEquals("Hashcodes are equal", layout1.hashCode(), layout2.hashCode());
        assertEquals("Layouts are equal", layout1, layout2);
    }
}
