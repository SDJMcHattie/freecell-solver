package uk.co.iscoding.freecell.game;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.co.iscoding.freecell.cards.PlayingCard;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: Stuart
 * Date: 27/05/2013
 * Time: 14:23
 */
public class FreeCellColumnTest {
    FreeCellColumn column;

    @Before
    public void setUp() throws Exception {
        column = new FreeCellColumn();
    }

    @After
    public void tearDown() throws Exception {
        column = null;
    }

    @Test
    public void testRankMustDescend() throws Exception {
        assertTrue("Can add a nine of hearts", column.addCard(PlayingCard.NINE_OF_HEARTS));
        assertFalse("Cannot add a ten of spades", column.addCard(PlayingCard.TEN_OF_SPADES));
        assertTrue("Can add eight of spades", column.addCard(PlayingCard.EIGHT_OF_SPADES));
        assertFalse("Cannot add a six of diamonds", column.addCard(PlayingCard.SIX_OF_DIAMONDS));
        assertTrue("Can add a seven of diamonds", column.addCard(PlayingCard.SEVEN_OF_DIAMONDS));
    }

    @Test
    public void testColoursMustAlternate() throws Exception {
        assertTrue("Can add a nine of hearts", column.addCard(PlayingCard.NINE_OF_HEARTS));
        assertFalse("Cannot add an eight of hearts", column.addCard(PlayingCard.EIGHT_OF_HEARTS));
        assertFalse("Cannot add an eight of diamonds", column.addCard(PlayingCard.EIGHT_OF_DIAMONDS));
        assertTrue("Can add eight of spades", column.addCard(PlayingCard.EIGHT_OF_SPADES));
        assertFalse("Cannot add a seven of spades", column.addCard(PlayingCard.SEVEN_OF_SPADES));
        assertFalse("Cannot add a seven of clubs", column.addCard(PlayingCard.SEVEN_OF_CLUBS));
        assertTrue("Can add seven of diamonds", column.addCard(PlayingCard.SEVEN_OF_DIAMONDS));
    }

    @Test
    public void testCannotCycle() throws Exception {
        assertTrue("Can add an ace of spades", column.addCard(PlayingCard.ACE_OF_SPADES));
        assertFalse("Cannot add a joker", column.addCard(PlayingCard.JOKER));
        assertFalse("Cannot add a king of spades", column.addCard(PlayingCard.KING_OF_SPADES));
        assertFalse("Cannot add a king of clubs", column.addCard(PlayingCard.KING_OF_CLUBS));
        assertFalse("Cannot add a king of hearts", column.addCard(PlayingCard.KING_OF_HEARTS));
        assertFalse("Cannot add a king of diamonds", column.addCard(PlayingCard.KING_OF_DIAMONDS));
    }
}
