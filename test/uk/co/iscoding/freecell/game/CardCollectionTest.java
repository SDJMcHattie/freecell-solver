package uk.co.iscoding.freecell.game;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import uk.co.iscoding.freecell.cards.PlayingCard;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static uk.co.iscoding.freecell.cards.PlayingCard.*;
import static uk.co.iscoding.freecell.game.CardCollection.SortMode;

/**
 * Created with IntelliJ IDEA.
 * User: Stuart
 * Date: 22/05/2013
 * Time: 22:40
 */
public class CardCollectionTest {
    CardCollection testCollection;

    @Before
    public void setUp() throws Exception {
        testCollection = new CardCollection(12);
    }

    @After
    public void tearDown() throws Exception {
        testCollection = null;
    }

    @Test
    public void testSetName() throws Exception {
        testCollection.setName("Testing");
        assertEquals("Name is set to 'Testing'", "Testing", testCollection.toString());
    }

    @Test
    public void testAddCardsWithoutRules() throws Exception {
        assertTrue("Can add card to collection", testCollection.addCard(ACE_OF_CLUBS));
        assertTrue("Can add another card to collection", testCollection.addCard(TWO_OF_HEARTS));
    }

    @Test
    public void testAddCardsWithAlternateColour() throws Exception {
        testCollection.suitMode = CardCollection.AllowedSuit.ALTERNATING_COLOUR;
        assertTrue("Can add any card to start", testCollection.addCard(KING_OF_DIAMONDS));
        assertFalse("Cannot add the same colour afterwards", testCollection.addCard(TWO_OF_HEARTS));
        assertTrue("Can add other colour afterwards", testCollection.addCard(NINE_OF_SPADES));
    }

    @Test
    public void testAddCardsInSequence() throws Exception {
        testCollection.rankMode = CardCollection.RankOrder.ASCENDING;
        assertTrue("Can add any card to start", testCollection.addCard(ACE_OF_CLUBS));
        assertFalse("Cannot add a non-ascending card", testCollection.addCard(KING_OF_DIAMONDS));
        assertTrue("Can add an ascending card afterwards", testCollection.addCard(TWO_OF_HEARTS));
        testCollection.rankMode = CardCollection.RankOrder.DESCENDING;
        assertTrue("Can add the original card in descending mode", testCollection.addCard(ACE_OF_CLUBS));
        assertFalse("Can't yet add the king without cycling", testCollection.addCard(KING_OF_DIAMONDS));
        testCollection.cycleMode = CardCollection.Cycle.CAN;
        assertTrue("Can now add the king", testCollection.addCard(KING_OF_DIAMONDS));
    }

    @Test
    public void testStartingRank() throws Exception {
        testCollection.startingRank = CardCollection.StartingRank.ACE;
        assertFalse("Can't add the king", testCollection.addCard(KING_OF_DIAMONDS));
        assertTrue("Can add the ace", testCollection.addCard(ACE_OF_CLUBS));
        assertTrue("Can now add the king", testCollection.addCard(KING_OF_DIAMONDS));
    }

    @Test
    public void testAddCollection() throws Exception {
        CardCollection secondCollection = new CardCollection(4);
        secondCollection.addCard(ACE_OF_CLUBS);
        secondCollection.addCard(NINE_OF_SPADES);

        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(TWO_OF_HEARTS);
        testCollection.addCollection(secondCollection);

        assertEquals("Correct number of cards after second collection added", 4, testCollection.getNumberOfCards());
        assertEquals("Top (last) card is 2", NINE_OF_SPADES, testCollection.getTopCard());
        assertEquals("Bottom (first) card is 3", KING_OF_DIAMONDS, testCollection.getBottomCard());
    }

    @Test
    public void testRemoveTopCard() throws Exception {
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(TWO_OF_HEARTS);
        testCollection.removeTopCard();
        assertEquals("Only one card left in the stack", 1, testCollection.getNumberOfCards());
        assertEquals("Only card is card 3", KING_OF_DIAMONDS, testCollection.getTopCard());
    }

    @Test
    public void testRemoveTopCards() throws Exception {
        testCollection.addCard(ACE_OF_CLUBS);
        testCollection.addCard(NINE_OF_SPADES);
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(TWO_OF_HEARTS);
        testCollection.removeTopCards(2);
        assertEquals("Only two cards left in the stack", 2, testCollection.getNumberOfCards());
        assertEquals("Top card is card 2", NINE_OF_SPADES, testCollection.getTopCard());
        assertEquals("Bottom card is card 1", ACE_OF_CLUBS, testCollection.getBottomCard());
    }

    @Test
    public void testRemoveCardAt() throws Exception {
        testCollection.addCard(ACE_OF_CLUBS);
        testCollection.addCard(NINE_OF_SPADES);
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(TWO_OF_HEARTS);
        testCollection.removeCardAt(2);
        assertEquals("Only three cards left in the stack", 3, testCollection.getNumberOfCards());
        assertEquals("Top card is card 4", TWO_OF_HEARTS, testCollection.getTopCard());
        assertEquals("Bottom card is card 1", ACE_OF_CLUBS, testCollection.getBottomCard());
        assertEquals("Other card is card 2", NINE_OF_SPADES, testCollection.getCard(1));
    }

    @Test
    public void testRemoveCard() throws Exception {
        testCollection.addCard(ACE_OF_CLUBS);
        testCollection.addCard(NINE_OF_SPADES);
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(TWO_OF_HEARTS);
        testCollection.removeCard(NINE_OF_SPADES);
        assertEquals("Only three cards left in the stack", 3, testCollection.getNumberOfCards());
        assertEquals("Top card is card 4", TWO_OF_HEARTS, testCollection.getTopCard());
        assertEquals("Bottom card is card 1", ACE_OF_CLUBS, testCollection.getBottomCard());
        assertEquals("Other card is card 3", KING_OF_DIAMONDS, testCollection.getCard(1));
    }

    @Test
    public void testGetIndexOf() throws Exception {
        testCollection.addCard(ACE_OF_CLUBS);
        testCollection.addCard(NINE_OF_SPADES);
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(TWO_OF_HEARTS);
        assertEquals("Index of card 3 is 2", 2, testCollection.getIndexOf(KING_OF_DIAMONDS));
    }

    @Test
    public void testGetBottomCard() throws Exception {
        testCollection.addCard(ACE_OF_CLUBS);
        testCollection.addCard(NINE_OF_SPADES);
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(TWO_OF_HEARTS);
        assertEquals("Bottom card is card 1", ACE_OF_CLUBS, testCollection.getBottomCard());
    }

    @Test
    public void testGetTopCard() throws Exception {
        testCollection.addCard(ACE_OF_CLUBS);
        testCollection.addCard(NINE_OF_SPADES);
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(TWO_OF_HEARTS);
        assertEquals("Bottom card is card 4", TWO_OF_HEARTS, testCollection.getTopCard());
    }

    @Test
    public void testGetTopCards() throws Exception {
        testCollection.addCard(ACE_OF_CLUBS);
        testCollection.addCard(NINE_OF_SPADES);
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(TWO_OF_HEARTS);
        CardCollection newCollection = testCollection.getTopCards(3);
        assertEquals("New collection has 3 cards", 3, newCollection.getNumberOfCards());
        assertEquals("New collection top card is card 4", TWO_OF_HEARTS, newCollection.getTopCard());
        assertEquals("New collection bottom card is card 2", NINE_OF_SPADES, newCollection.getBottomCard());
        assertEquals("New collection middle card is card 3", KING_OF_DIAMONDS, newCollection.getCard(1));
    }

    @Test
    public void testSortCards() throws Exception {
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(NINE_OF_SPADES);
        testCollection.addCard(ACE_OF_CLUBS);
        testCollection.addCard(TWO_OF_HEARTS);
        testCollection.sortCards(SortMode.ASCENDING);
        assertEquals("Ascending bottom card is card 1", ACE_OF_CLUBS, testCollection.getBottomCard());
        assertEquals("Ascending top card is card 3", KING_OF_DIAMONDS, testCollection.getTopCard());
        testCollection.sortCards(SortMode.DESCENDING);
        assertEquals("Descending bottom card is card 3", KING_OF_DIAMONDS, testCollection.getBottomCard());
        assertEquals("Descending top card is card 1", ACE_OF_CLUBS, testCollection.getTopCard());
    }

    @Test
    public void testIsAlternatingColour() throws Exception {
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.addCard(NINE_OF_SPADES);
        testCollection.addCard(TWO_OF_HEARTS);
        testCollection.addCard(ACE_OF_CLUBS);
        assertTrue("Alternating cards are listed as alternating", testCollection.isAlternatingColour());
        testCollection.removeCardAt(2);
        assertFalse("Non-alternating cards are listed as such", testCollection.isAlternatingColour());
    }

    @Test
    public void testIsDescendingRank() throws Exception {
        testCollection.addCard(TWO_OF_HEARTS);
        testCollection.addCard(ACE_OF_CLUBS);
        testCollection.addCard(KING_OF_DIAMONDS);
        testCollection.cycleMode = CardCollection.Cycle.CAN;
        assertFalse("Cycling cannot make a collection descending", testCollection.isDescendingRank());
        testCollection.removeCardAt(2);
        assertTrue("Descending rank comes back as descending", testCollection.isDescendingRank());
    }
}
