package uk.co.iscoding.freecell.game;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static uk.co.iscoding.freecell.cards.PlayingCard.*;
import static uk.co.iscoding.freecell.game.CardCollection.StartingSuit;

/**
 * Created with IntelliJ IDEA.
 * User: Stuart
 * Date: 24/05/2013
 * Time: 12:50
 */
public class FoundationTest {
    Foundation found;

    @Before
    public void setUp() throws Exception {
        found = new Foundation(StartingSuit.HEART);
    }

    @After
    public void tearDown() throws Exception {
        found = null;
    }

    @Test
    public void testFoundationOnlyAcceptsCorrectSuit() throws Exception {
        assertFalse("Cannot add spades", found.addCard(ACE_OF_SPADES));
        assertFalse("Cannot add joker", found.addCard(JOKER));
        assertTrue("Can add ace of Hearts", found.addCard(ACE_OF_HEARTS));
    }

    @Test
    public void testFoundationOnlyAcceptsAceToStart() throws Exception {
        assertFalse("Cannot add king", found.addCard(KING_OF_HEARTS));
        assertFalse("Cannot add queen", found.addCard(QUEEN_OF_HEARTS));
        assertFalse("Cannot add jack", found.addCard(JACK_OF_HEARTS));
        assertFalse("Cannot add ten", found.addCard(TEN_OF_HEARTS));
        assertFalse("Cannot add nine", found.addCard(NINE_OF_HEARTS));
        assertFalse("Cannot add eight", found.addCard(EIGHT_OF_HEARTS));
        assertFalse("Cannot add seven", found.addCard(SEVEN_OF_HEARTS));
        assertFalse("Cannot add six", found.addCard(SIX_OF_HEARTS));
        assertFalse("Cannot add five", found.addCard(FIVE_OF_HEARTS));
        assertFalse("Cannot add four", found.addCard(FOUR_OF_HEARTS));
        assertFalse("Cannot add three", found.addCard(THREE_OF_HEARTS));
        assertFalse("Cannot add two", found.addCard(TWO_OF_HEARTS));
        assertTrue("Can add ace of Hearts", found.addCard(ACE_OF_HEARTS));
    }

    @Test
    public void testFoundationOnlyAcceptsSequential() throws Exception {
        assertTrue("Can add ace of Hearts", found.addCard(ACE_OF_HEARTS));
        assertFalse("Cannot add king", found.addCard(KING_OF_HEARTS));
        assertFalse("Cannot add queen", found.addCard(QUEEN_OF_HEARTS));
        assertFalse("Cannot add jack", found.addCard(JACK_OF_HEARTS));
        assertFalse("Cannot add ten", found.addCard(TEN_OF_HEARTS));
        assertFalse("Cannot add nine", found.addCard(NINE_OF_HEARTS));
        assertFalse("Cannot add eight", found.addCard(EIGHT_OF_HEARTS));
        assertFalse("Cannot add seven", found.addCard(SEVEN_OF_HEARTS));
        assertFalse("Cannot add six", found.addCard(SIX_OF_HEARTS));
        assertFalse("Cannot add five", found.addCard(FIVE_OF_HEARTS));
        assertFalse("Cannot add four", found.addCard(FOUR_OF_HEARTS));
        assertFalse("Cannot add three", found.addCard(THREE_OF_HEARTS));
        assertTrue("Can add two", found.addCard(TWO_OF_HEARTS));
    }

    @Test
    public void testFoundationAcceptsAllCardsInSuit() throws Exception {
        assertTrue("Can add ace", found.addCard(ACE_OF_HEARTS));
        assertTrue("Can add two", found.addCard(TWO_OF_HEARTS));
        assertTrue("Can add three", found.addCard(THREE_OF_HEARTS));
        assertTrue("Can add four", found.addCard(FOUR_OF_HEARTS));
        assertTrue("Can add five", found.addCard(FIVE_OF_HEARTS));
        assertTrue("Can add six", found.addCard(SIX_OF_HEARTS));
        assertTrue("Can add seven", found.addCard(SEVEN_OF_HEARTS));
        assertTrue("Can add eight", found.addCard(EIGHT_OF_HEARTS));
        assertTrue("Can add nine", found.addCard(NINE_OF_HEARTS));
        assertTrue("Can add ten", found.addCard(TEN_OF_HEARTS));
        assertTrue("Can add jack", found.addCard(JACK_OF_HEARTS));
        assertTrue("Can add queen", found.addCard(QUEEN_OF_HEARTS));
        assertTrue("Can add king", found.addCard(KING_OF_HEARTS));
        assertFalse("Cannot add ace again", found.addCard(ACE_OF_HEARTS));
    }

}
