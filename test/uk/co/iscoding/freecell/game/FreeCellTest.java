package uk.co.iscoding.freecell.game;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static uk.co.iscoding.freecell.cards.PlayingCard.*;

/**
 * Created with IntelliJ IDEA.
 * User: Stuart
 * Date: 27/05/2013
 * Time: 14:10
 */
public class FreeCellTest {
    FreeCell testCell;

    @Before
    public void setUp() throws Exception {
        testCell = new FreeCell();
    }

    @After
    public void tearDown() throws Exception {
        testCell = null;
    }

    @Test
    public void testIsOccupied() throws Exception {
        assertFalse("New FreeCell is empty", testCell.isOccupied());
        testCell.addCard(ACE_OF_DIAMONDS);
        assertTrue("FreeCell is not empty after adding a card", testCell.isOccupied());
    }

    @Test
    public void testCellWillTakeAnyCard() throws Exception {
        assertTrue("FreeCell accepts Ace of Diamonds", testCell.addCard(ACE_OF_DIAMONDS));
        testCell.removeCardAt(0);
        assertTrue("FreeCell accepts Four of Hearts", testCell.addCard(FOUR_OF_HEARTS));
        testCell.removeCardAt(0);
        assertTrue("FreeCell accepts Seven of Spades", testCell.addCard(SEVEN_OF_SPADES));
        testCell.removeCardAt(0);
        assertTrue("FreeCell accepts Jack of Clubs", testCell.addCard(JACK_OF_CLUBS));
        testCell.removeCardAt(0);
        assertTrue("FreeCell accepts Joker", testCell.addCard(JOKER));
    }

    @Test
    public void testCellTakesOnlyOneCard() throws Exception {
        assertTrue("New FreeCell accepts the first card", testCell.addCard(ACE_OF_DIAMONDS));
        assertFalse("FreeCell with a card won't accept another", testCell.addCard(FOUR_OF_HEARTS));
    }
}
