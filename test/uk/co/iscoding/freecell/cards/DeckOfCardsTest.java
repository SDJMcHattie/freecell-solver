package uk.co.iscoding.freecell.cards;

import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: Stuart
 * Date: 21/05/2013
 * Time: 23:17
 */
public class DeckOfCardsTest {

    DeckOfCards deck;

    @Before
    public void setup() {
        deck = new DeckOfCards();
    }

    @After
    public void tearDown() {
        deck = null;
    }

    private static ArrayList<Integer> getCardsInSequence() {
        ArrayList<Integer> cardNums = Lists.newArrayList();

        for (int i = 1; i <= 52; i++) {
            cardNums.add(i);
        }

        return cardNums;
    }

    private ArrayList<Integer> getDeckOrder(DeckOfCards deck) {
        ArrayList<Integer> cardNums = Lists.newArrayList();

        for (int i = 0; i < 52; i++) {
            cardNums.add(deck.nextCard().getCardNumber());
        }

        return cardNums;
    }

    private ArrayList<Integer> getMSShuffle1234Order() {
        return Lists.newArrayList(13, 10, 12, 37, 17, 51, 3, 29, 9, 18, 38, 27, 50, 6, 45, 39,
                44, 5, 25, 41, 30, 35, 34, 22, 28, 36, 49, 1, 43, 26, 52, 31, 4, 8, 16, 2, 23,
                15, 14, 47, 32, 21, 33, 7, 20, 48, 42, 40, 46, 24, 19, 11);
    }

    @Test
    public void testNewDeckOrder() {
        assertEquals("New deck is in numerical order", getCardsInSequence(), getDeckOrder(deck));
    }

    @Test
    public void testShuffle() {
        deck.shuffle();
        assertNotEquals("Shuffled deck is NOT in numerical order", getCardsInSequence(), getDeckOrder(deck));
    }

    @Test
    public void testMSShuffle() {
        deck.msShuffle(1234);
        assertEquals("Microsoft shuffle 1234 of deck is correct", getMSShuffle1234Order(), getDeckOrder(deck));
    }

}
