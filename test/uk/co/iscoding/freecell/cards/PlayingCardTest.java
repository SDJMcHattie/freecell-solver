package uk.co.iscoding.freecell.cards;

import org.junit.Test;

import static org.junit.Assert.*;
import static uk.co.iscoding.freecell.cards.PlayingCard.*;


/**
 * Created with IntelliJ IDEA.
 * User: Stuart
 * Date: 22/05/2013
 * Time: 21:53
 */
public class PlayingCardTest {
    @Test
    public void testGetSuit() {
        assertEquals("Joker is Suit.JOKER", Suit.JOKER, JOKER.getSuit());
        assertEquals("Ace of Spades is Suit.SPADES", Suit.SPADES, ACE_OF_SPADES.getSuit());
        assertEquals("Three of Hearts is Suit.HEARTS", Suit.HEARTS, THREE_OF_HEARTS.getSuit());
        assertEquals("Five of Diamonds is Suit.DIAMONDS", Suit.DIAMONDS, FIVE_OF_DIAMONDS.getSuit());
        assertEquals("King of Clubs is Suit.CLUBS", Suit.CLUBS, KING_OF_CLUBS.getSuit());
    }

    @Test
    public void testGetColour() {
        assertEquals("Joker is Colour.NONE", Colour.NONE, JOKER.getColour());
        assertEquals("Ace of Spades is Colour.BLACK", Colour.BLACK, ACE_OF_SPADES.getColour());
        assertEquals("Three of Hearts is Colour.RED", Colour.RED, THREE_OF_HEARTS.getColour());
        assertEquals("Five of Diamonds is Colour.RED", Colour.RED, FIVE_OF_DIAMONDS.getColour());
        assertEquals("King of Clubs is Colour.BLACK", Colour.BLACK, KING_OF_CLUBS.getColour());
    }

    @Test
    public void testGetRank() {
        assertEquals("Joker is Rank.JOKER", Rank.JOKER, JOKER.getRank());
        assertEquals("Ace of Spades is Rank.ACE", Rank.ACE, ACE_OF_SPADES.getRank());
        assertEquals("Three of Hearts is Rank.THREE", Rank.THREE, THREE_OF_HEARTS.getRank());
        assertEquals("Five of Diamonds is Rank.FIVE", Rank.FIVE, FIVE_OF_DIAMONDS.getRank());
        assertEquals("King of Clubs is Rank.KING", Rank.KING, KING_OF_CLUBS.getRank());
    }

    @Test
    public void testIsSameSuitAs() {
        assertTrue("Same suit cards are recognised by isSameSuitAs", NINE_OF_DIAMONDS.isSameSuitAs(FIVE_OF_DIAMONDS));
        assertFalse("Different suit cards are recognised by isSameSuitAs", KING_OF_CLUBS.isSameSuitAs(ACE_OF_SPADES));
        assertFalse("Joker is different suit to hearts", JOKER.isSameSuitAs(THREE_OF_HEARTS));
        assertFalse("Clubs is different suit to Joker", KING_OF_CLUBS.isSameSuitAs(JOKER));
        assertTrue("Joker is the same suit as a Joker", JOKER.isSameSuitAs(JOKER));
    }

    @Test
    public void testIsSameColourAs() {
        assertTrue("Same colour cards are recognised by isSameColourAs", NINE_OF_DIAMONDS.isSameColourAs(THREE_OF_HEARTS));
        assertFalse("Different colour cards are recognised by isSameColourAs", KING_OF_CLUBS.isSameColourAs(THREE_OF_HEARTS));
        assertFalse("Joker is a different colour than hearts", JOKER.isSameColourAs(THREE_OF_HEARTS));
        assertFalse("Clubs is a different colour than Joker", KING_OF_CLUBS.isSameColourAs(JOKER));
        assertTrue("Joker is the same colour as a Joker", JOKER.isSameColourAs(JOKER));
    }

    @Test
    public void testRankDifference() {
        assertEquals("Rank difference is positive", 2, FIVE_OF_DIAMONDS.rankDifference(THREE_OF_HEARTS));
        assertEquals("Rank difference is negative", -12, ACE_OF_SPADES.rankDifference(KING_OF_CLUBS));
        assertEquals("Rank difference with Joker is -128", -128, KING_OF_CLUBS.rankDifference(JOKER));
        assertEquals("Rank difference of Joker is -128", -128, JOKER.rankDifference(NINE_OF_DIAMONDS));
        assertEquals("Rank difference of identical cards is zero", 0, NINE_OF_DIAMONDS.rankDifference(NINE_OF_DIAMONDS));
        assertEquals("Rank difference of two Joker is zero", 0, JOKER.rankDifference(JOKER));
    }

    @Test
    public void testGetCardNumber() {
        assertEquals("Ace of Spades is card number 4", 4, ACE_OF_SPADES.getCardNumber());
        assertEquals("Five of Diamonds is card number 18", 18, FIVE_OF_DIAMONDS.getCardNumber());
        assertEquals("King of Clubs is card number 49", 49, KING_OF_CLUBS.getCardNumber());
        assertEquals("Joker is card number 53", 53, JOKER.getCardNumber());
    }

    @Test
    public void testToString() {
        assertEquals("Ace of Spades card name is correct", "Ace of Spades", ACE_OF_SPADES.toString());
        assertEquals("Three of Hearts card name is correct", "Three of Hearts", THREE_OF_HEARTS.toString());
        assertEquals("Five of Diamonds card name is correct", "Five of Diamonds", FIVE_OF_DIAMONDS.toString());
        assertEquals("King of Clubs card name is correct", "King of Clubs", KING_OF_CLUBS.toString());
        assertEquals("Joker card name is correct", "Joker", JOKER.toString());
    }

    @Test
    public void testDeepClone() {
        PlayingCard clone = FIVE_OF_DIAMONDS.deepClone();
        assertTrue("Clone is equivalent card", FIVE_OF_DIAMONDS.equals(clone));
        assertFalse("Clone is not the same card", FIVE_OF_DIAMONDS == clone);
    }

    @Test
    public void testEquals() {
        assertTrue("Equivalent cards are equal", FIVE_OF_DIAMONDS.equals(new PlayingCard(Suit.DIAMONDS, Rank.FIVE)));
        assertFalse("Non equivalent cards are not equal", KING_OF_CLUBS.equals(FIVE_OF_DIAMONDS));
        assertFalse("Joker is not equivalent to normal card", NINE_OF_DIAMONDS.equals(JOKER));
        assertTrue("Joker is equivalent to another Joker", JOKER.equals(new PlayingCard(Suit.JOKER, Rank.JOKER)));
    }
}
