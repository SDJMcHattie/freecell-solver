/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.iscoding.freecell.cards;

import com.google.common.base.Objects;

/**
 *
 * @author   Stuart David James McHattie
 * @version  1.0 2011-06-29
 * @since    2011-06-29
 */
public class PlayingCard {
    public static final PlayingCard ACE_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.ACE);
    public static final PlayingCard TWO_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.TWO);
    public static final PlayingCard THREE_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.THREE);
    public static final PlayingCard FOUR_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.FOUR);
    public static final PlayingCard FIVE_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.FIVE);
    public static final PlayingCard SIX_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.SIX);
    public static final PlayingCard SEVEN_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.SEVEN);
    public static final PlayingCard EIGHT_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.EIGHT);
    public static final PlayingCard NINE_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.NINE);
    public static final PlayingCard TEN_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.TEN);
    public static final PlayingCard JACK_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.JACK);
    public static final PlayingCard QUEEN_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.QUEEN);
    public static final PlayingCard KING_OF_CLUBS = new PlayingCard(Suit.CLUBS, Rank.KING);
    public static final PlayingCard ACE_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.ACE);
    public static final PlayingCard TWO_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.TWO);
    public static final PlayingCard THREE_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.THREE);
    public static final PlayingCard FOUR_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.FOUR);
    public static final PlayingCard FIVE_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.FIVE);
    public static final PlayingCard SIX_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.SIX);
    public static final PlayingCard SEVEN_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.SEVEN);
    public static final PlayingCard EIGHT_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.EIGHT);
    public static final PlayingCard NINE_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.NINE);
    public static final PlayingCard TEN_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.TEN);
    public static final PlayingCard JACK_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.JACK);
    public static final PlayingCard QUEEN_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.QUEEN);
    public static final PlayingCard KING_OF_DIAMONDS = new PlayingCard(Suit.DIAMONDS, Rank.KING);
    public static final PlayingCard ACE_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.ACE);
    public static final PlayingCard TWO_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.TWO);
    public static final PlayingCard THREE_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.THREE);
    public static final PlayingCard FOUR_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.FOUR);
    public static final PlayingCard FIVE_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.FIVE);
    public static final PlayingCard SIX_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.SIX);
    public static final PlayingCard SEVEN_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.SEVEN);
    public static final PlayingCard EIGHT_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.EIGHT);
    public static final PlayingCard NINE_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.NINE);
    public static final PlayingCard TEN_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.TEN);
    public static final PlayingCard JACK_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.JACK);
    public static final PlayingCard QUEEN_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.QUEEN);
    public static final PlayingCard KING_OF_HEARTS = new PlayingCard(Suit.HEARTS, Rank.KING);
    public static final PlayingCard ACE_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.ACE);
    public static final PlayingCard TWO_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.TWO);
    public static final PlayingCard THREE_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.THREE);
    public static final PlayingCard FOUR_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.FOUR);
    public static final PlayingCard FIVE_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.FIVE);
    public static final PlayingCard SIX_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.SIX);
    public static final PlayingCard SEVEN_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.SEVEN);
    public static final PlayingCard EIGHT_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.EIGHT);
    public static final PlayingCard NINE_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.NINE);
    public static final PlayingCard TEN_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.TEN);
    public static final PlayingCard JACK_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.JACK);
    public static final PlayingCard QUEEN_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.QUEEN);
    public static final PlayingCard KING_OF_SPADES = new PlayingCard(Suit.SPADES, Rank.KING);
    public static final PlayingCard JOKER = new PlayingCard(Suit.JOKER, Rank.JOKER);

    public enum Rank {
        JOKER("Joker", 0),
        ACE("Ace", 1),
        TWO("Two", 2),
        THREE("Three", 3),
        FOUR("Four", 4),
        FIVE("Five", 5),
        SIX("Six", 6),
        SEVEN("Seven", 7),
        EIGHT("Eight", 8),
        NINE("Nine", 9),
        TEN("Ten", 10),
        JACK("Jack", 11),
        QUEEN("Queen", 12),
        KING("King", 13);

        public final String stringRepresentation;
        public final int value;

        private Rank(String stringRepresentation, int value) {
            this.stringRepresentation = stringRepresentation;
            this.value = value;
        }
    }

    public enum Suit {
        JOKER("Joker", 56),
        CLUBS("Clubs", 0),
        DIAMONDS("Diamonds", 1),
        HEARTS("Hearts", 2),
        SPADES("Spades", 3);

        public final String stringRepresentation;
        public final int deckOffset;

        private Suit(String stringRepresentation, int deckOffset) {
            this.stringRepresentation = stringRepresentation;
            this.deckOffset = deckOffset;
        }
    }

    public enum Colour {
        NONE,
        RED,
        BLACK,
    }

    private final Suit suit;
    private final Rank rank;

    public PlayingCard(Suit suit, Rank rank) {
        if (suit == Suit.JOKER || rank == Rank.JOKER) {
            this.suit = Suit.JOKER;
            this.rank = Rank.JOKER;
        } else {
            this.suit = suit;
            this.rank = rank;
        }
    }

    public Suit getSuit() {
        return suit;
    }

    Colour getColour() {
        switch (suit) {
            case HEARTS:
            case DIAMONDS:
                return Colour.RED;
            case CLUBS:
            case SPADES:
                return Colour.BLACK;
            case JOKER:
            default:
                return Colour.NONE;
        }
    }

    public Rank getRank() {
        return rank;
    }

    public boolean isSameSuitAs(PlayingCard card) {
        return suit == card.getSuit();
    }

    public boolean isSameColourAs(PlayingCard card) {
        return getColour() == card.getColour();
    }

    public byte rankDifference(PlayingCard card) {
        Rank cardRank = card.getRank();
        if ((cardRank == Rank.JOKER || rank == Rank.JOKER) && rank != cardRank) {
            return -128;
        } else {
            return (byte) (rank.value - cardRank.value);
        }
    }

    public int getCardNumber() {
        return getRank().value * 4 - 3 + getSuit().deckOffset;
    }

    @Override
    public String toString() {
        if (getSuit() == Suit.JOKER) {
            return "Joker";
        } else {
            return String.format("%s of %s", getRank().stringRepresentation, getSuit().stringRepresentation);
        }
    }

    public PlayingCard deepClone() {
        return new PlayingCard(getSuit(), getRank());
    }

    public boolean equals(PlayingCard card) {
        return toString().equals(card.toString());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(suit, rank);
    }
}
