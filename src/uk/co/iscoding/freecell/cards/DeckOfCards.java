/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.iscoding.freecell.cards;

import uk.co.iscoding.freecell.cards.PlayingCard.Rank;
import uk.co.iscoding.freecell.cards.PlayingCard.Suit;
import uk.co.iscoding.freecell.random.SRandom;

import java.util.ArrayList;



/**
 *
 * @author   Stuart David James McHattie
 * @version  1.0 2011-06-29
 * @since    2011-06-29
 */
public class DeckOfCards {

    private ArrayList<PlayingCard> deck;
    private byte currentCard = 0;

    public DeckOfCards() {
        deck = new ArrayList<PlayingCard>(52);

        Suit[] suits = {Suit.CLUBS, Suit.DIAMONDS, Suit.HEARTS, Suit.SPADES};
        Rank[] ranks = {Rank.ACE, Rank.TWO, Rank.THREE, Rank.FOUR, Rank.FIVE, Rank.SIX, Rank.SEVEN,
                        Rank.EIGHT, Rank.NINE, Rank.TEN, Rank.JACK, Rank.QUEEN, Rank.KING};
        for (Rank rank : ranks) {
            for (Suit suit : suits) {
                deck.add(new PlayingCard(suit, rank));
            }
        }
    }

    public void shuffle() {
        PlayingCard temp;
        for (int i = deck.size() - 1; i > 0; i--) {
            int j = (int) Math.floor(Math.random() * i);
            temp = deck.get(i);
            deck.set(i, deck.get(j));
            deck.set(j, temp);
        }
    }

    public void msShuffle(int seed) {
        ArrayList<PlayingCard> newDeck = new ArrayList<PlayingCard>(52);
        SRandom rand = new SRandom(seed);
        int j, wLeft = 52;

        for (int i = 0; i < 52; i++) {
            j = rand.nextRand() % wLeft;
            newDeck.add(deck.get(j));
            deck.set(j, deck.get(--wLeft));
        }

        deck = newDeck;
    }

    public PlayingCard nextCard() {
        if (currentCard < deck.size()) {
            return deck.get(currentCard++);
        } else {
            return null;
        }
    }

    public void display() {
        System.out.println("Displaying Deck:");
        for (PlayingCard card : deck) {
            System.out.println(card.toString());
        }
        System.out.println();
    }
}
