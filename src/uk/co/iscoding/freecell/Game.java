/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.iscoding.freecell;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Sets;
import uk.co.iscoding.freecell.cards.DeckOfCards;
import uk.co.iscoding.freecell.game.FreeCellLayout;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author   Stuart David James McHattie
 * @version  1.0 2011-06-30
 * @since    2011-06-30
 */
public class Game {
    private final DeckOfCards deck = new DeckOfCards();
    private final Set<FreeCellLayout> pastLayouts = Sets.newHashSet();
    private Set<FreeCellLayout> currentLayouts = Sets.newHashSet();
    private int iterationCounter = 1;

    private Game(int msGame) {
        deck.msShuffle(msGame);
        currentLayouts.add(new FreeCellLayout(deck));
    }

    /**
     * Solve the game
     */
    public void solve() {
        Stopwatch stopwatch = new Stopwatch().start();

        /*******************************************************
         * Identify possible moves after the current positions *
         *******************************************************/
        Set<FreeCellLayout> newLayouts = Sets.newHashSet();

        for (FreeCellLayout layout : currentLayouts) {
            newLayouts.addAll(layout.getPossibleMoves());
        }

        currentLayouts = newLayouts;


        /*********************************************************************************
         * Check which of the resulting positions have already been seen and remove them *
         *********************************************************************************/
        currentLayouts.removeAll(pastLayouts);


        /********************************************************************
         * If no moves are left, print that the game is impossible and exit *
         ********************************************************************/
        if (currentLayouts.size() == 0) {
            System.out.println("\nThe game has no solution that could be found.");
            System.exit(-1);
        }


        /************************************************************************************
         * If any moves are left, add their resulting layouts to those which have been seen *
         ************************************************************************************/
        pastLayouts.addAll(currentLayouts);


        /***************************************************************************************************
         * Check if any of the current positions are winners and, if they are, print the solution and exit
         ***************************************************************************************************/
        for (FreeCellLayout layout : currentLayouts) {
            if (layout.isFinished()) {
                System.out.println();
                layout.printMoves();
                System.exit(0);
            }
        }

        System.out.println(String.format("Game Depth: %03d   New Layouts: %03d   Total Layouts: %05d   Iteration Time: %dms",
                iterationCounter++, currentLayouts.size(), pastLayouts.size(), stopwatch.elapsed(TimeUnit.MILLISECONDS)));

        /*******************************
         * Call the solve method again *
         *******************************/
        solve();
    }

    public static void main(String[] args) {
        System.out.println();
        Game game = new Game(30);
        game.solve();
    }
}
