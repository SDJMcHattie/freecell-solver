/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.iscoding.freecell.game;

/**
 *
 * @author   Stuart David James McHattie
 * @version  1.0 2011-07-05
 * @since    2011-07-05
 */
class FreeCellColumn extends CardCollection {
    public FreeCellColumn() {
        super(19);
        rankMode = RankOrder.DESCENDING;
        startingRank = StartingRank.ANY;
        startingSuit = StartingSuit.ANY;
        suitMode = AllowedSuit.ALTERNATING_COLOUR;
        cycleMode = Cycle.CANNOT;
    }

    public FreeCellColumn deepClone() {
        FreeCellColumn clone = new FreeCellColumn();
        populateDeepClone(clone);
        return clone;
    }
}
