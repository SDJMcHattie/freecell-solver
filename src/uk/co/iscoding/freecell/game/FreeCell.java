/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.iscoding.freecell.game;

/**
 *
 * @author   Stuart David James McHattie
 * @version  1.0 2011-07-05
 * @since    2011-07-05
 */
class FreeCell extends CardCollection {

    public FreeCell() {
        super(1);
        rankMode = RankOrder.ANY;
        suitMode = AllowedSuit.ANY;
    }

    public boolean isOccupied() {
        return getNumberOfCards() > 0;
    }

    public FreeCell deepClone() {
        FreeCell clone = new FreeCell();
        populateDeepClone(clone);
        return clone;
    }
}
