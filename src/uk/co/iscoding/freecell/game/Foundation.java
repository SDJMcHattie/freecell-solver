/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.iscoding.freecell.game;

/**
 *
 * @author   Stuart David James McHattie
 * @version  1.0 2011-07-05
 * @since    2011-07-05
 */
class Foundation extends CardCollection {

    public Foundation(StartingSuit suit) {
        super(13);
        suitMode = AllowedSuit.SAME_SUIT;
        rankMode = RankOrder.ASCENDING;
        startingRank = StartingRank.ACE;
        startingSuit = suit;
        cycleMode = Cycle.CANNOT;
    }

    public Foundation deepClone() {
        Foundation clone = new Foundation(startingSuit);
        populateDeepClone(clone);
        return clone;
    }
}
