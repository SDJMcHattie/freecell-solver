/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.iscoding.freecell.random;

/**
 *
 * @author   Stuart David James McHattie
 * @version  1.0 2011-06-30
 * @since    2011-06-30
 */
public class SRandom {
    private static final int RAND_MAX = 32767; // 0x7FFF

    private int seed = 0;

    public SRandom(int newSeed) {
        seed = newSeed;
    }

    public int nextRand() {
        seed = seed * 214013 + 2531011;
        return (seed >> 16) & RAND_MAX;
    }
}
